import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {
  @HostBinding('class.width-100') class = true;

  constructor() { }

  ngOnInit() {
  }
}
