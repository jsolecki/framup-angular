import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { ErrorComponent } from './error/error.component';
import { MasterComponent } from './shared/layout/master/master.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () =>
      import('./modules/authentication/authentication.module').then(
        (m) => m.AuthenticationModule,
      ),
  },
  {
    path: '',
    component: MasterComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/product/product.module').then((m) => m.ProductModule),
      },
      {
        path: '',
        loadChildren: () =>
          import('./modules/category/category.module').then((m) => m.CategoryModule),
      },
    ],
  },
  { path: '**', redirectTo: 'error' },
  {
    path: 'error',
    component: MasterComponent,
    children: [
      {
        path: '',
        component: ErrorComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
