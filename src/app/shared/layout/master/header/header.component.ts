import { IAuthUser } from '@/modules/authentication/interfaces/AuthUser';
import { AuthenticationService } from '@/modules/authentication/services/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public currentUser: IAuthUser;
  constructor(private auth: AuthenticationService) { }
  ngOnInit() {
    if(this.auth.currentUserValue) {
      this.currentUser = this.auth.currentUserValue;
    }
  }
  logout() {
    this.auth.logout();
  }
}
