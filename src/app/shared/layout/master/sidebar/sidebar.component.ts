import { ICategory } from '@/modules/category/interfaces/category';
import { CategoryService } from '@/modules/category/services/category.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  public categories: ICategory[] = [];
  constructor(private category: CategoryService) { }

  ngOnInit() {
    this.category.getCategories().subscribe((res) => {
      this.categories = res.categories;
    });
  }
}
