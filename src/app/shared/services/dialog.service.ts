import { Injectable } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';

import { DialogComponent } from '../components/dialog/dialog.component';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(public dialog: MatDialog) {}

  public async openDialog(name: string, caption: string, data: any = null): Promise<string> {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '600px',
      data: { name, caption, data },
    });

    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe((result) => {
        if(!result) {
          return;
        }
        resolve(result.data);
      });
    });
  }
}
