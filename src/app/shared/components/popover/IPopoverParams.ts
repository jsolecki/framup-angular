import { PopoverContent } from './popover-ref';

export interface IPopoverParams<T> {
  origin: HTMLElement;
  content: PopoverContent;
  data?: T;
  width?: string | number;
  height?: string | number;
}
