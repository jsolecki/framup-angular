import { HeaderComponent } from '@//shared/layout/master/header/header.component';
import { MasterComponent } from '@//shared/layout/master/master.component';
import { SidebarComponent } from '@//shared/layout/master/sidebar/sidebar.component';
import { ProductComponent } from '@/modules/product/components/product/product.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
// material
import { MatPaginatorModule, MatTableModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';

import { DialogComponent } from './components/dialog/dialog.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { PopoverComponent } from './components/popover/popover.component';
import { InsidePopoverComponent } from './components/inside-popover/inside-popover.component';

@NgModule({
  declarations: [
    MasterComponent,
    HeaderComponent,
    SidebarComponent,
    SpinnerComponent,
    ProductComponent,
    DialogComponent,
    PopoverComponent,
    InsidePopoverComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,

    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatTabsModule,
    MatCheckboxModule,
    MatGridListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDialogModule,
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    MasterComponent,
    HeaderComponent,
    SidebarComponent,
    SpinnerComponent,
    ProductComponent,
    DialogComponent,

    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatTabsModule,
    MatCheckboxModule,
    MatGridListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDialogModule,
  ],
  entryComponents: [
    DialogComponent,
    PopoverComponent,
    InsidePopoverComponent,
  ],
})
export class SharedModule {}
