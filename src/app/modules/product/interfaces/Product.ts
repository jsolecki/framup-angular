export interface IProduct {
    productId: string;
    title: string;
    description: string;
    products?: string[];
    // tslint:disable-next-line: naming-convention
    _id?: string;
    promoted: boolean;
    imageUrl: string;
}
