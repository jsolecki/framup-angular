import { AuthGuard } from '@/modules/authentication/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateComponent } from './views/admin/products/create/create.component';
import { EditComponent } from './views/admin/products/edit/edit.component';
import { IndexComponent } from './views/admin/products/index/index.component';
import { ProductListComponent } from './views/products/product-list/product-list.component';
import { ProductShowComponent } from './views/products/product-show/product-show.component';

const base_path = 'products';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ProductListComponent,
      },
      {
        path: 'show/:productId',
        component: ProductShowComponent,
      },
    ],
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: base_path,
        component: IndexComponent,
      },
      {
        path: `${base_path}/create`,
        component: CreateComponent,
      },
      {
        path: `${base_path}/edit/:id`,
        component: EditComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRoutingModule {}
