import { ChangeDetectorRef } from '@angular/core';
import { IndexComponent } from './index.component';
import { MatDialog } from '@angular/material';
import { of } from 'rxjs';
import { ComponentFixture } from '@angular/core/testing';

describe('Product', () => {
  describe('IndexComponent', () => {
    let component: IndexComponent;
    let PRODUCTS;
    let mockProductService;
    let mockDialogService;
    let mockSnackBarService;
    let changeDetectorRef: ChangeDetectorRef;
    let dialog: MatDialog;

    beforeEach(() => {
      PRODUCTS = [
        { id: '1', title: 'product 1' },
        { id: '2', title: 'product 2' },
        { id: '3', title: 'product 3' },
      ];

      mockProductService = jasmine.createSpyObj(['getProducts', 'removeProduct']);
      mockDialogService = jasmine.createSpyObj(['openDialog']);
      mockSnackBarService = jasmine.createSpyObj(['openSnackBar']);

      component = new IndexComponent(
        changeDetectorRef,
        mockDialogService,
        mockSnackBarService,
        mockProductService,
        dialog,
      );
    });

    describe('getProducts', () => {
      it('should get products', () => {
        // mockProductService.getProducts.and.returnValue(of(true));
        // component.getProducts();

        // expect(mockProductService.getProducts).toHaveBeenCalled();
        expect(true).toBe(true);
      });
    });
  });
});
