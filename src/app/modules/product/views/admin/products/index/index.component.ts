import { ProductService } from '@/modules/product/services/product.service';
import { DialogService } from '@/shared/services/dialog.service';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { timeouts } from 'src/timeouts';

import { IProduct } from '../../../../interfaces/Product';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexComponent implements OnInit {
  @HostBinding('class.width-100') class = true;
  displayedColumns: string[] = [
    'imageUrl',
    '_id',
    'title',
    'categoryId',
    'price',
    'actions',
  ];
  dataSource: MatTableDataSource<IProduct>;
  productsLoaded: boolean = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private changeDetectorRef: ChangeDetectorRef,
              private dialogService: DialogService,
              private snackBarService: SnackBarService,
              public productService: ProductService,
              public dialog: MatDialog,
  ) {
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productsLoaded = false;
    this.productService.getProducts().subscribe(
      (res) => {
        this.dataSource = new MatTableDataSource(res.products);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.productsLoaded = true;
          this.changeDetectorRef.detectChanges();
        }, timeouts.default);
      },
      (err) => console.log(err, 'res'),
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  async openDialog(id: string, title: string) {
    const result = await this.dialogService.openDialog(
      'Confirmation',
      `Are you sure to delete product: <b>${title}</b>`,
      id,
    );

    if (!result) {
      return;
    }

    this.productService.removeProduct(result).subscribe((res) => {
      this.snackBarService.openSnackBar(`Product ${title} has been removed`);
      this.getProducts();
      this.changeDetectorRef.detectChanges();
    });
  }
}
