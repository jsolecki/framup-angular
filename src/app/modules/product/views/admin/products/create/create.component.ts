import { ICategory } from '@/modules/category/interfaces/category';
import { CategoryService } from '@/modules/category/services/category.service';
import { ProductService } from '@/modules/product/services/product.service';
import { FormErrorsMatcher } from '@/shared/libs/FormErrorsMatcher';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateComponent implements OnInit {
    @HostBinding('class.width-100') class = true;
    form: FormGroup;
    matcher = new FormErrorsMatcher();

    public categories: ICategory[] = [];

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private fb: FormBuilder,
        private router: Router,
        private categoryService: CategoryService,
        private snackBarService: SnackBarService,
        public productService: ProductService,
        ) { }

    ngOnInit() {
        this.createForm();
        this.categoryService.getCategories().subscribe(({categories}) => {
            this.categories = categories;
            this.changeDetectorRef.detectChanges();
        });
    }

    createForm() {
        this.form = this.fb.group({
            title: ['', [Validators.required, Validators.minLength(2)]],
            description: [''],
            price: ['', [Validators.required]],
            amount: ['', [Validators.required]],
            imageUrl: [''],
            categoryId: ['', [Validators.required]],
            promoted: [''],
        });
    }
    onSubmit() {
        if (!this.form.valid) {
            return;
        }
            this.productService.storeProduct(this.form.value).subscribe((res) => {
                this.snackBarService.openSnackBar(`Product ${res.product.title} has been successfully created`, 'X');
                this.changeDetectorRef.detectChanges();
                this.router.navigate([this.productService.getRoute('index')]);
            }, (err) => console.log(err, 'err'));
    }
}
