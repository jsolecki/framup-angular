import { ICategory } from '@/modules/category/interfaces/category';
import { CategoryService } from '@/modules/category/services/category.service';
import { ProductService } from '@/modules/product/services/product.service';
import { FormErrorsMatcher } from '@/shared/libs/FormErrorsMatcher';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { keys, pick } from 'lodash';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditComponent implements OnInit {
  @HostBinding('class.width-100') class = true;
  form: FormGroup;
  resetPassForm: FormGroup;

  matcher = new FormErrorsMatcher();

  public categories: ICategory[] = [];

    constructor(private changeDetectorRef: ChangeDetectorRef,
                private fb: FormBuilder,
                private router: Router,
                private route: ActivatedRoute,
                private categoryService: CategoryService,
                private snackBarService: SnackBarService,
                public productService: ProductService,
  ) {}

  async ngOnInit() {
    this.createForm();
    this.categoryService.getCategories().subscribe(({ categories }) => {
      this.categories = categories;
      this.changeDetectorRef.detectChanges();
    });
    this.productService
      .findProduct(this.route.snapshot.params.id)
      .subscribe(({ product }) => {
        this.form.setValue(pick(product, keys(this.form.value)));
        this.changeDetectorRef.detectChanges();
      });
  }

  createForm() {
    this.form = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(2)]],
      description: [''],
      price: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      imageUrl: [''],
      categoryId: ['', [Validators.required]],
      promoted: [''],
    });
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    this.productService
      .updateProduct({
        _id: this.route.snapshot.params.id,
        ...this.form.value,
      })
      .subscribe(
        (res) => {
          this.snackBarService.openSnackBar(
            `Product ${res.product.title} has been successfully updated`,
            'X',
          );
          this.changeDetectorRef.detectChanges();
          this.router.navigate([this.productService.getRoute('index')]);
        },
        (err) => console.log(err, 'err'),
      );
  }
}
