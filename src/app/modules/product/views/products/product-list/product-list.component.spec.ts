import { ProductListComponent } from "./product-list.component"
import { of } from 'rxjs';

describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let PRODUCTS;
  let mockProductService;

  beforeEach(() => {
    PRODUCTS = [
      { _id: '1', title: 'product 1' },
      { _id: '2', title: 'product 2' },
      { _id: '3', title: 'product 3' },
    ];

    mockProductService = jasmine.createSpyObj(['getProducts']);

    component = new ProductListComponent(mockProductService);
  });

  it('should set products correctly from the service', () => {

    mockProductService.getProducts.and.returnValue(of(PRODUCTS));
    component.products = PRODUCTS;

    expect(component.products.length).toBe(PRODUCTS.length);
  })
})