import { IProduct } from '@/modules/product/interfaces/Product';
import { ProductService } from '@/modules/product/services/product.service';
import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from "@angular/core/testing"
import { of } from 'rxjs';

import { ProductListComponent } from './product-list.component';
import { By } from '@angular/platform-browser';

describe('ProductListComponent', () => {
  let fixture: ComponentFixture<ProductListComponent>;
  let mockProductService;
  let PRODUCTS;

  @Component({
    selector: 'app-product',
    template: '<div></div>',
  })
  class FakeProductComponent {
    @Input() model: IProduct;
    isOpen = false;
  }

  @Component({
    selector: 'app-spinner',
    template: '<div></div>',
  })
  class SpinnerComponent {
    @Input() spinning: boolean = false;
  }

  beforeEach(() => {
    mockProductService = jasmine.createSpyObj(['getProducts', 'removeProduct']);
    
    PRODUCTS = [
      { _id: '1', title: 'product 1' },
      { _id: '2', title: 'product 2' },
      { _id: '3', title: 'product 3' },
    ];


    TestBed.configureTestingModule({
      declarations: [
        ProductListComponent,
        FakeProductComponent,
        SpinnerComponent,
      ],
      providers: [
        { provide: ProductService, useValue: mockProductService },
      ],
    });
    fixture = TestBed.createComponent(ProductListComponent);
  });
  it('should set products correctly from the service', () => {
    mockProductService.getProducts.and.returnValue(of({products: PRODUCTS}));
    fixture.detectChanges();

    expect(fixture.componentInstance.products.length).toBe(PRODUCTS.length);
  });

  it('should create one article for each product', () => {
    mockProductService.getProducts.and.returnValue(of({products: PRODUCTS}));
    fixture.detectChanges();

    expect(fixture.debugElement.queryAll(By.css('.products__item')).length).toBe(PRODUCTS.length);
  });
});
