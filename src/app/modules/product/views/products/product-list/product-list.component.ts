import { IProduct } from '@/modules/product/interfaces/Product';
import { ProductService } from '@/modules/product/services/product.service';
import { Component, HostBinding, OnInit } from '@angular/core';
import { timeouts } from 'src/timeouts';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  @HostBinding('class.width-100') class = true;
  public products: IProduct[] = [];
  public productsLoaded: boolean = false;
  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes() {
    this.productService.getProducts()
      .subscribe(({products}) => this.products = products);
    // this.productService
    // .getProducts()
    // .subscribe((res: { products: IProduct[] }) => {
    //   this.products = res.products;
    //   setTimeout(() => {
    //     this.productsLoaded = true;
    //   }, timeouts.default);
    // });
  }
}
