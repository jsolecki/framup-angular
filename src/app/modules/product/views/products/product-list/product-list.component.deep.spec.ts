import { ProductComponent } from './../../../components/product/product.component';
import { ProductService } from '@/modules/product/services/product.service';
import { NO_ERRORS_SCHEMA, Directive, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { forEach } from 'lodash';
import { of } from 'rxjs';

import { ProductListComponent } from './product-list.component';
import { SpinnerComponent } from '@/shared/components/spinner/spinner.component';
import { MatIconModule } from '@angular/material';
import { By } from '@angular/platform-browser';

@Directive({
  selector: '[routerLink]',
  host: { '(click)': 'onClick()' },
})
export class RouterLinkDirectiveSub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;
  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

describe('ProductListComponentDeep', () => {
  let mockProductService;
  let fixture: ComponentFixture<ProductListComponent>;
  let PRODUCTS;

  beforeEach(() => {
    mockProductService = jasmine.createSpyObj(['getProducts']);
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
      ],
      declarations: [
        ProductListComponent,
        RouterLinkDirectiveSub,
        ProductComponent,
        SpinnerComponent,
      ],
      providers: [
        { provide: ProductService, useValue: mockProductService },
      ],
      // schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(ProductListComponent);
    PRODUCTS = [
      { _id: '1', title: 'product 1', imageUrl: '/assets/img/car3.jpg' },
      { _id: '2', title: 'product 2', imageUrl: '/assets/img/car3.jpg' },
      { _id: '3', title: 'product 3', imageUrl: '/assets/img/car3.jpg' },
    ];
  });
  it('should render each product as ProductComponent', () => {
    mockProductService.getProducts.and.returnValue(of({products: PRODUCTS}));
    fixture.detectChanges();

    forEach(fixture.componentInstance.products, (item, index) => {
      expect(PRODUCTS[index]._id).toEqual(item._id);
    });
  });

  it('should have the correct route for the first hero', () => {
    mockProductService.getProducts.and.returnValue(of({products: PRODUCTS}));
    fixture.detectChanges();

    const productComponents = fixture.debugElement.queryAll(By.directive(ProductComponent));

    const routerLink = productComponents[0].query(By.directive(RouterLinkDirectiveSub)).injector.get(RouterLinkDirectiveSub);

    productComponents[0].query(By.css('a')).triggerEventHandler('click', null);

    expect(routerLink.navigatedTo).toBe(`/show/${PRODUCTS[0]._id}`);
  });
});
