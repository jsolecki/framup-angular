import { IProduct } from '@/modules/product/interfaces/Product';
import { ProductService } from '@/modules/product/services/product.service';
import { PopoverService } from '@/shared/components/popover/popover.service';
import { Component, HostBinding, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-product-show',
  templateUrl: './product-show.component.html',
  styleUrls: ['./product-show.component.scss'],
})
export class ProductShowComponent implements OnInit {
  product$: Observable<IProduct>;
  @HostBinding('class.width-100') class = true;
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private popoverService: PopoverService,
  ) {}
  ngOnInit() {
    setTimeout(() => {
      this.product$ = this.productService
      .findProduct(this.route.snapshot.paramMap.get('productId'))
      .pipe(map((res) => res.product));
    }, 250); // setTimeout only for tests
  }

  show(content: TemplateRef<any>, origin) {
    const ref = this.popoverService.open<{skills: number[]}>(
      {
        content,
        //  content: 'Hello world!',
        // content: InsidePopoverComponent,
        origin,
        width: '200px',
        data: {
          skills: [1, 2, 3],
        },
      },
    );

    ref.afterClosed$.subscribe((res) => {
      console.log(res);
    });
  }
}
