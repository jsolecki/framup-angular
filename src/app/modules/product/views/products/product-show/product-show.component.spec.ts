import { ComponentFixture, TestBed, fakeAsync, tick, flush } from "@angular/core/testing"
import { ProductShowComponent } from './product-show.component'
import { ProductService } from '@/modules/product/services/product.service';
import { PopoverService } from '@/shared/components/popover/popover.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('ProductShow', () => {
  let fixture: ComponentFixture<ProductShowComponent>;
  let mockProductService;
  let mockPopoverService;
  let mockActivatedRoute;
  beforeEach(() => {
    mockProductService = jasmine.createSpyObj(['findProduct']);
    mockPopoverService = jasmine.createSpyObj(['open']);
    mockActivatedRoute = {
      snapshot: { paramMap: { get: () => '3' } },
    };

    TestBed.configureTestingModule({
      declarations: [ProductShowComponent],
      providers: [
        { provide: ProductService, useValue: mockProductService },
        { provide: PopoverService, useValue: mockPopoverService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
      ],
    });

    fixture = TestBed.createComponent(ProductShowComponent);
  });

  it('should fetch correct data', fakeAsync(() => {
    mockProductService.findProduct.and.returnValue(of({}));
    fixture.detectChanges();
    // tick(300);
    flush();
    expect(mockProductService.findProduct).toHaveBeenCalled();
  }));
})