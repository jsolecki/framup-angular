import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { ProductService } from './product.service';

describe('ProductService', () => {
  let httpTestingController: HttpTestingController;
  let service: ProductService;
  let productModel;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService],
    });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ProductService);
    productModel = {
      id: '5d6e41a88fd59b2acc95e23a',
      amount: 1,
      description: 'sample desc',
      price: '23$',
      imageUrl: '/assets/img/car3.jpg',
      promoted: false,
      title: 'sample title 2',
      categoryId: '5d6e41a58a9977046cf869a6',
      __v: 0,
    };
  });

  describe('findProduct', () => {
    it('should call get with the correct URL', () => {
      service.findProduct(productModel.id).subscribe((product) => {
        expect(product.title).toEqual(productModel.title);
      });

      const req = httpTestingController.expectOne(`products/${productModel.id}`);
      // const req2 = httpTestingController.expectOne(`products/${productModel.id}`);

      req.flush(productModel);

      httpTestingController.verify();
      expect(true).toBe(true);
    });
  });
});
