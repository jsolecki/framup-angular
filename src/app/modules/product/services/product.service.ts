import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { IProduct } from '../interfaces/Product';

import { routes } from './../module';

@Injectable({
    providedIn: 'root',
})
export class ProductService {
    constructor(private http: HttpClient) { }

    public getProducts() {
        return this.http.get<any>(`products`);
    }

    public storeProduct(product: IProduct) {
        return this.http.post<any>(`products`, product);
    }

    // tslint:disable-next-line: naming-convention
    public updateProduct({_id, ...product}: IProduct) {
        return this.http.put<any>(`products/${_id}`, product);
    }

    public findProduct(id: string) {
        return this.http.get<any>(`products/${id}`);
    }
    public removeProduct(id: string) {
        return this.http.delete<any>(`products/${id}`);
    }

    public getRoute(key: string) {
        return routes[key];
    }
}
