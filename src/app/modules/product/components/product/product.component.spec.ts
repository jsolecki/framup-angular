import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IProduct } from '../../interfaces/Product';

import { ProductComponent } from './product.component';

describe('ProductComponent', () => {
  let fixture: ComponentFixture<ProductComponent>;
  let product: IProduct;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [BrowserAnimationsModule],
    });
    product = {
      _id: '1',
      description: 'simple description',
      title: 'Product1',
      imageUrl: 'http://catalog.wlimg.com/4/385210/small-images/car-coach-rental-services-38131.jpg',
      promoted: true,
      productId: '2',
    };

    fixture = TestBed.createComponent(ProductComponent);
  });
  it('should have the correct product', () => {
    fixture.componentInstance.model = product;
    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.css('h2')).nativeElement.textContent,
    ).toContain(product.title);

    expect(fixture.nativeElement.querySelector('h2').textContent).toContain(
      product.title,
    );
  });
});
