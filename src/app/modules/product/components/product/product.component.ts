import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { timeouts } from 'src/timeouts';

import { IProduct } from '../../interfaces/Product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          opacity: 1,
          bottom: '0px',
        }),
      ),
      state(
        'closed',
        style({
          opacity: 0.5,
          bottom: '-10px',
        }),
      ),

      transition('closed => open', [animate('0.3s')]),
    ]),
  ],
})
export class ProductComponent implements OnInit {
  @Input() model: IProduct;
  isOpen = false;

  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      this.isOpen = true;
    }, timeouts.default);
  }
}
