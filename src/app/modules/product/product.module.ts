import { SharedModule } from '@/shared/shared.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProductRoutingModule } from './product-routing.module';
import { CreateComponent } from './views/admin/products/create/create.component';
import { EditComponent } from './views/admin/products/edit/edit.component';
import { IndexComponent } from './views/admin/products/index/index.component';
import { ProductListComponent } from './views/products/product-list/product-list.component';
import { ProductShowComponent } from './views/products/product-show/product-show.component';

@NgModule({
  declarations: [
    IndexComponent,
    CreateComponent,
    EditComponent,
    ProductListComponent,
    ProductShowComponent,
  ],
  imports: [RouterModule, SharedModule, ProductRoutingModule],
})
export class ProductModule {}
