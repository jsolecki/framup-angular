export const routes = {
  index: '/admin/products',
  create: '/admin/products/create',
  edit: '/admin/products/edit',
};
