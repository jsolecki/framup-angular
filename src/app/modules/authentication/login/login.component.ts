import { FormErrorsMatcher } from '@/shared/libs/FormErrorsMatcher';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { REGEX_PATTERNS } from 'src/regexPatterns';
import { NOT_FOUND, UNAUTHORIZED } from 'src/response';

import { AuthenticationService } from '../services/authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    matcher = new FormErrorsMatcher();
    errorMessage: string = '';

    constructor(private fb: FormBuilder,
                private auth: AuthenticationService,
                private router: Router,
                private snackBarService: SnackBarService,
        ) { }

    ngOnInit() {
        this.createForm();
    }
    onSubmit() {
        if (!this.loginForm.valid) {
            return;
        }
        this.auth.login(this.loginForm.value)
            .subscribe((res) => {
                this.snackBarService.openSnackBar('Successfully logged in', 'X');
                this.router.navigate(['/']);
            },
            ({error}) => {
                switch(error.status) {
                    case UNAUTHORIZED: this.loginForm.get('password').setErrors({incorrect: true}); break;
                    case NOT_FOUND: this.loginForm.get('email').setErrors({notFound: true}); break;
                    default: this.snackBarService.openSnackBar(error.message, 'Error');
                }
                this.errorMessage = error.message;
            });
    }

    createForm() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.validEmail)]],
            password: ['', [Validators.required]],
        });
    }
}
