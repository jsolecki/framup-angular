import { SharedModule } from '@/shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { AuthenticationRoutingModule } from './routes/authentication-routing.module';
import { RegisterComponent } from './register/register.component';

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
    ],
    imports: [
        CommonModule,
        AuthenticationRoutingModule,
        SharedModule,
    ],
})
export class AuthenticationModule { }
