export class IAuthUser {
    email: string;
    token: string;
    exp: string;
    iat: string;
    userId: string;
}
