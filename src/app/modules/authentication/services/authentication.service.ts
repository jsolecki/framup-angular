import { SnackBarService } from '@/shared/services/snack-bar.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IAuthUser } from '../interfaces/AuthUser';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  public get currentUserValue() {
    return this.currentUserSubject$.getValue();
  }
  public currentUser$: Observable<IAuthUser>;
  private currentUserSubject$: BehaviorSubject<IAuthUser>;

  constructor(
    private http: HttpClient,
    private snackBarService: SnackBarService,
  ) {
    this.currentUserSubject$ = new BehaviorSubject<IAuthUser>(
      JSON.parse(localStorage.getItem('currentUser')),
    );
    this.currentUser$ = this.currentUserSubject$.asObservable();
  }
  public login(form: { email: string; password: string }) {
    return this.http.post<any>(`login`, form).pipe(
      map((res) => {
        // login successful if there's a jwt token in the response
        if (res.token) {
          const base64 = res.token.split('.')[1];

          const user = { ...JSON.parse(window.atob(base64)), token: res.token };

          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject$.next(user);
        }

        return res;
      }),
    );
  }

  public register(form: { email: string; password: string }) {
    return this.http.post<any>(`sign-in`, form);
  }

  public logout() {
    localStorage.removeItem('currentUser');
    this.snackBarService.openSnackBar('You have been logged out', 'X');
    this.currentUserSubject$.next(null);
  }
}
