import { AuthenticationService } from '@/modules/authentication/services/authentication.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UNAUTHORIZED } from 'src/response';

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService,
                private router: Router,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((err) => {
            if (err.status === UNAUTHORIZED || err.error.message === 'jwt expired') {
                this.authenticationService.logout();
                this.router.navigate(['auth/login']);
            }

            return throwError(err);
        }));
    }
}
