import { FormErrorsMatcher } from '@/shared/libs/FormErrorsMatcher';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { REGEX_PATTERNS } from 'src/regexPatterns';
import { CONFLICT } from 'src/response';

import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    matcher = new FormErrorsMatcher();
    errorMessage: string = '';

    constructor(private fb: FormBuilder,
                private auth: AuthenticationService,
                private router: Router,
                private snackBarService: SnackBarService,
        ) { }

    ngOnInit() {
        this.createForm();
    }
    onSubmit() {
        if (!this.registerForm.valid) {
            return;
        }
        this.auth.register(this.registerForm.value)
            .subscribe((res) => {
                this.snackBarService.openSnackBar('Successfully registered. Now you can log in', 'X');
                this.router.navigate(['/auth/login']);
            },
            ({error}) => {
                switch(error.status) {
                    case CONFLICT: this.registerForm.get('email').setErrors({alreadyExists: true}); break;
                    default: this.snackBarService.openSnackBar(error.message, 'Error');
                }
                this.errorMessage = error.message;
            });
    }

    createForm() {
        this.registerForm = this.fb.group({
            name: ['', [Validators.required]],
            surname: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern(REGEX_PATTERNS.validEmail)]],
            password: ['', [Validators.required]],
            password_confirm: ['', [Validators.required]],
        });
    }

}
