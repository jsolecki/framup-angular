export const routes = {
  index: '/admin/categories',
  create: '/admin/categories/create',
  edit: '/admin/categories/edit',
};
