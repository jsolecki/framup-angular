import { AuthenticationService } from '@/modules/authentication/services/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ICategory } from '../interfaces/category';

import { routes } from './../module';

@Injectable({
    providedIn: 'root',
})
export class CategoryService {
    constructor(private http: HttpClient, private auth: AuthenticationService) { }

    public getCategories() {
        return this.http.get<any>('categories');
    }

    public storeCategory(category: ICategory) {
        return this.http.post<any>('categories', {userId: this.auth.currentUserValue.userId, ...category});
    }

    // tslint:disable-next-line: naming-convention
    public updateCategory({_id, ...category}: ICategory) {
        return this.http.put<any>(`categories/${_id}`, category);
    }

    public findCategory(id: string) {
        return this.http.get<any>(`categories/${id}`);
    }

    public removeCategory(id: string) {
        return this.http.delete<any>(`categories/${id}`);
    }

    public getRoute(key: string) {
        return routes[key];
    }
}
