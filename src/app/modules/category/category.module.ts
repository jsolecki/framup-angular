import { SharedModule } from '@/shared/shared.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CategoryRoutingModule } from './category-routing.module';
import { CreateComponent } from './views/admin/categories/create/create.component';
import { EditComponent } from './views/admin/categories/edit/edit.component';
import { IndexComponent } from './views/admin/categories/index/index.component';
import { CategoryShowComponent } from './views/categories/category-show/category-show.component';

@NgModule({
  declarations: [
      IndexComponent,
      CreateComponent,
      EditComponent,
      CategoryShowComponent,
  ],
  imports: [
    RouterModule,
    SharedModule,
    CategoryRoutingModule,
  ],
})
export class CategoryModule { }
