export interface ICategory {
    categoryId?: string;
    title: string;
    description: string;
    products?: string[];
    // tslint:disable-next-line: naming-convention
    _id?: string;
}
