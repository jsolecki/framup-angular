import { AuthGuard } from '@/modules/authentication/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateComponent } from './views/admin/categories/create/create.component';
import { EditComponent } from './views/admin/categories/edit/edit.component';
import { IndexComponent } from './views/admin/categories/index/index.component';
import { CategoryShowComponent } from './views/categories/category-show/category-show.component';

const base_path = 'categories';

const routes: Routes = [
  {
    path: 'category/:categoryId',
    component: CategoryShowComponent,
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    children: [
      {
        path: base_path,
        component: IndexComponent,
      },
      {
        path: `${base_path}/create`,
        component: CreateComponent,
      },
      {
        path: `${base_path}/edit/:id`,
        component: EditComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoryRoutingModule {}
