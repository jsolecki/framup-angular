import { DialogService } from '@/shared/services/dialog.service';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ICategory } from '../../../../interfaces/category';
import { CategoryService } from '../../../../services/category.service';
import { timeouts } from 'src/timeouts';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndexComponent implements OnInit {
  @HostBinding('class.width-100') class = true;
  displayedColumns: string[] = ['_id', 'title', 'description', 'actions'];
  dataSource: MatTableDataSource<ICategory>;
  categoriesLoaded: boolean = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private changeDetectorRef: ChangeDetectorRef,
              private snackBarService: SnackBarService,
              private dialogService: DialogService,
              public categoryService: CategoryService,
  ) {
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.categoriesLoaded = false;
    this.categoryService.getCategories().subscribe(
      (res) => {
        this.dataSource = new MatTableDataSource(res.categories);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        setTimeout(() => {
          this.categoriesLoaded = true;
          this.changeDetectorRef.detectChanges();
        }, timeouts.default);
      },
      (err) => console.log(err, 'res'),
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  async openDialog(id: string, title: string) {
    const result = await this.dialogService.openDialog(
      'Confirmation',
      `Are you sure to delete product: <b>${title}</b>`,
      id,
    );

    if (!result) {
      return;
    }

    this.categoryService.removeCategory(result).subscribe((res) => {
      this.snackBarService.openSnackBar(`Category ${title} has been removed`);
      this.getCategories();
      this.changeDetectorRef.detectChanges();
    });
  }
}
