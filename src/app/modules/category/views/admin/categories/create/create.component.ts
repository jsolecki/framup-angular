import { CategoryService } from '@/modules/category/services/category.service';
import { FormErrorsMatcher } from '@/shared/libs/FormErrorsMatcher';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateComponent implements OnInit {
  form: FormGroup;
  matcher = new FormErrorsMatcher();
  @HostBinding('class.width-100') class = true;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private snackBarService: SnackBarService,
    public categoryService: CategoryService,
    ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      title: [''],
      description: [''],
    });
  }
  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    this.categoryService.storeCategory(this.form.value).subscribe(
      (res) => {
        this.changeDetectorRef.detectChanges();
        this.snackBarService.openSnackBar(
            'Category has been successfully updated',
            'X',
          );
          this.router.navigate([this.categoryService.getRoute('index')]);
      },
      (err) => console.log(err));
  }
}
