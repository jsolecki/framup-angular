import { CategoryService } from '@/modules/category/services/category.service';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { CreateComponent } from './create.component'
import { By } from '@angular/platform-browser';
import { ICategory } from '@/modules/category/interfaces/category';
import { of } from 'rxjs';

describe('Category', () => {
  describe('create', () => {
    let fixture: ComponentFixture<CreateComponent>;
    let mockCategoryService;
    let mockRouter;
    let mockSnackBar;

    beforeEach(() => {
      mockCategoryService = jasmine.createSpyObj(['storeCategory', 'getRoute']);
      mockRouter = jasmine.createSpyObj(['navigate']);
      mockSnackBar = jasmine.createSpyObj(['openSnackBar']);

      TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule,
        ],
        declarations: [
          CreateComponent,
        ],
        providers: [
          { provide: CategoryService, useValue: mockCategoryService },
          { provide: Router, useValue: mockRouter },
          { provide: SnackBarService, useValue: mockSnackBar },
        ],
        schemas: [NO_ERRORS_SCHEMA],
      });
      fixture = TestBed.createComponent(CreateComponent);
    });

    it('should add a new category when button is clicked', () => {
      const newCategory: ICategory = {
        title: 'new category',
        description: 'new description',
      };

      mockCategoryService.storeCategory.and.returnValue(of(newCategory));

      // const inputName = fixture.debugElement.query(By.css('[formcontrolname="title"]')).nativeElement;
      // const inputDesc = fixture.debugElement.query(By.css('[formcontrolname="description"]')).nativeElement;

      // inputName.value = newCategory.title;
      // inputDesc.value = newCategory.description;
      fixture.detectChanges();
      
      fixture.componentInstance.form.setValue(newCategory);
      
      fixture.detectChanges();
      fixture.componentInstance.onSubmit();

      expect(mockCategoryService.storeCategory).toHaveBeenCalledWith(newCategory);
      expect(mockSnackBar.openSnackBar).toHaveBeenCalled();

    })
  })
})