import { CategoryService } from '@/modules/category/services/category.service';
import { FormErrorsMatcher } from '@/shared/libs/FormErrorsMatcher';
import { SnackBarService } from '@/shared/services/snack-bar.service';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  OnInit,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { keys, pick } from 'lodash';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditComponent implements OnInit {
  form: FormGroup;
  resetPassForm: FormGroup;

  matcher = new FormErrorsMatcher();

  @HostBinding('class.width-100') class = true;
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService,
    public categoryService: CategoryService,
  ) {}

  ngOnInit() {
    this.createForm();
    this.categoryService
      .findCategory(this.route.snapshot.params.id)
      .subscribe(({ category }) => {
        this.form.setValue(pick(category, keys(this.form.value)));
      });
  }

  createForm() {
    this.form = this.fb.group({
      title: [''],
      description: [''],
    });
  }
  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    this.categoryService
      .updateCategory({
        _id: this.route.snapshot.params.id,
        ...this.form.value,
      })
      .subscribe(
        (res) => {
          this.snackBarService.openSnackBar(
            'Category has been successfully updated',
            'X',
          );
          this.changeDetectorRef.detectChanges();
          this.router.navigate([this.categoryService.getRoute('index')]);
        },
        (err) => console.log(err, 'err'));
  }
}
