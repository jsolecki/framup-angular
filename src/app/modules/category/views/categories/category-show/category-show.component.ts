import { ICategory } from '@/modules/category/interfaces/Category';
import { CategoryService } from '@/modules/category/services/category.service';
import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { timeouts } from 'src/timeouts';

@Component({
  selector: 'app-category-show',
  templateUrl: './category-show.component.html',
  styleUrls: ['./category-show.component.scss'],
})
export class CategoryShowComponent implements OnInit {
  @HostBinding('class.width-100') class = true;
  public category$: Observable<ICategory>;
  public productsLoaded: boolean = false;
  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute,
  ) {}
  ngOnInit() {
    this.route.params.subscribe(({ categoryId }) => {
      this.productsLoaded = false;
      this.category$ = this.categoryService
        .findCategory(categoryId)
        .pipe(map((res) => res.category));
      setTimeout(() => {
        this.productsLoaded = true;
      }, timeouts.default);
    });
  }
}
